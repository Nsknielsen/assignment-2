# assignment-2

## Komputer Store
Simple vanilla JavaScript store website.
Has functionality that pretends to work, deposit money in the bank, take out and repay loans, as well as options to read about different laptops and buy them.

#To use
Can be opened in VS Code live server. Is also hosted on Gitlab pages at https://nsknielsen.gitlab.io/assignment-2/.

# Dependencies
Nothing
