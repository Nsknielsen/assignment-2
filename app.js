//DOM ELEMENT CONSTANTS//
const bankBalanceElement = document.getElementById('bankBalance');
const outstandingLoanElement = document.getElementById('outstandingLoan');
const repayLoanElement = document.getElementById('repayLoan');
const loanButtonElement = document.getElementById('loan');
const payBalanceElement = document.getElementById('payBalance');
const bankButtonElement = document.getElementById('bank');
const workButtonElement = document.getElementById('work');
const laptopListElement = document.getElementById('laptops');
const priceElement = document.getElementById('price');
const buyButtonElement = document.getElementById('buy');
const laptopFeaturesElement = document.getElementById('features');
const laptopImageElement = document.getElementById('laptopImage');
const laptopNameElement = document.getElementById('laptopName');
const laptopDescElement = document.getElementById('laptopDesc')

//GLOBAL JAVASCRIPT VARIABLES//
let laptops = [];
let payBalance = 0;
let bankBalance = 200;
let outstandingLoan = 0;
//Constants used in loan calculation and repayment.
const loanMaxFactor = 2;
const loanPaymentRate = 10;


//LAPTOPS SECTION//
//Setup and data fetching

//Fetches data from web API
fetch('https://noroff-komputer-store-api.herokuapp.com/computers')
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToMenu(laptops))
    .catch (error => console.log(error));

//Adds data into the HTML.
const addLaptopsToMenu = (laptops) => {
    laptops.forEach(laptop => addLaptopToMenu(laptop));
    handleLaptopMenuChange();
}

//Adds data about each laptop into html laptops select element.
const addLaptopToMenu = (laptop) => {
    const laptopElement = document.createElement('option');
    laptopElement.value = laptop.id;
    laptopElement.innerText = laptop.title;
    laptopListElement.appendChild(laptopElement);
}

//Laptops event handlers
//Updates the laptops section when a new laptop is selected from the menu.
const handleLaptopMenuChange = () => {
    const selectedLaptop = laptops[laptopListElement.selectedIndex];
    priceElement.innerText = selectedLaptop.price + ' kr.';
    laptopNameElement.innerText = selectedLaptop.title;
    laptopDescElement.innerText = selectedLaptop.description;
    const imageUrl = 'https://noroff-komputer-store-api.herokuapp.com/' + selectedLaptop.image;
    laptopImageElement.src = imageUrl;
    laptopFeaturesElement.innerText = '';
    for (spec of selectedLaptop.specs) {
        let listElement = laptopFeaturesElement.appendChild(document.createElement("li"));
        listElement.innerText = spec;
    }
}

//Checks if the user can afford a laptop, and if it is in stock, and buys it if possible.
const handleBuyButton = () => {
    const selectedLaptop = laptops[laptopListElement.selectedIndex];
    const price = selectedLaptop.price;
    if (price > bankBalance) {
        alert('You cannot afford this computer')
    } else if (selectedLaptop.stock <= 0) {
        alert('Sorry, this laptop is out of stock')
    } else {
        bankBalance -= price;
        selectedLaptop.stock -= 1;
        updateBalanceElements();
        alert('Congratulations, you just bought a "new" ' + selectedLaptop.title);
    }
}

//Laptops event listeners
laptopListElement.addEventListener('change', handleLaptopMenuChange);
buyButtonElement.addEventListener('click', handleBuyButton);


//WORK SECTION//
//Work event handlers
//Increments pay balance.
const handleWorkButton = () => {
    payBalance += 100;
    updateBalanceElements();
}

//Transfers from pay balance to bank balance, paying off up to 10% of any outstanding loan first.
const handleBankButton = () => {
    if (hasOutstandingLoan()) {
        const loanPayment = payBalance / loanPaymentRate;
        if (loanPayment >= outstandingLoan) {
            payBalance -= outstandingLoan;
            outstandingLoan = 0;
        } else {
            outstandingLoan -= loanPayment;
            payBalance -= loanPayment;
        }
    }
    bankBalance += payBalance;
    payBalance = 0;
    updateBalanceElements();
}

//Repays as much as possible of loan using pay balance.
const handleRepayLoanButton = () => {
    if (hasOutstandingLoan()) {
        if (outstandingLoan >= payBalance) {
            outstandingLoan -= payBalance;
            payBalance = 0;
        } else {
            payBalance -= outstandingLoan;
            outstandingLoan = 0;
            bankBalance += payBalance;
            payBalance = 0;
        }
    }
    updateBalanceElements();
}

//Work event listeners
workButtonElement.addEventListener('click', handleWorkButton);
repayLoanElement.addEventListener('click', handleRepayLoanButton);
bankButtonElement.addEventListener('click', handleBankButton);

//BANK SECTION//
//Bank event handler
//Checks if the user can take out a loan, and adds the loan if possible.
const handleLoanButton = () => {
    if (hasOutstandingLoan()) {
        alert('Sorry, you already have a loan');
    } else {
        let loanAmount = Number(prompt('How much do you want to loan?'));
        if (isNaN(loanAmount)) {
            alert('Invalid loan input, stop messing around');
        } else if (loanAmount > bankBalance * loanMaxFactor) {
            alert('You need more money in the bank to get such a big loan');
        } else {
            outstandingLoan = loanAmount;
            bankBalance += loanAmount;
            outstandingLoanElement.hidden = false;
            repayLoanElement.hidden = false;
            updateBalanceElements();
        }
    }
}
//Bank event listener
loanButtonElement.addEventListener('click', handleLoanButton);

//HELPER FUNCTIONS//
//Checks if the user already has a loan. Used various places to decide how to allocate money when buttons are clicked.
//This is a quite short helper function, but I included it A) because the phrase "hasOustandingLoan" is more obvious to understand than a variable
//comparison, and B) because you might in a real-life scenario eventually get a case with more complex criteria for loan eligibility.
const hasOutstandingLoan = () => {
    if (outstandingLoan > 0) {
        return true;
    }
}

//Updates all balance elements in the DOM to their current values, and hides the loan features when appropriate.
//Is sometimes called when only 1 or 2 of the balance values have been changed, 
//but I preferred the ease of having just one update function.
const updateBalanceElements = () => {
    payBalanceElement.innerText = payBalance + " kr.";
    bankBalanceElement.innerText = bankBalance + " kr.";
    if (outstandingLoan === 0){
        outstandingLoanElement.hidden = true;
        repayLoanElement.hidden = true;
    } else {
        outstandingLoanElement.innerText = "Oustanding loan: " + outstandingLoan + " kr.";
    }
}
